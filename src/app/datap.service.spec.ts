import { TestBed, inject } from '@angular/core/testing';

import { DatapService } from './datap.service';

describe('DatapService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DatapService]
    });
  });

  it('should be created', inject([DatapService], (service: DatapService) => {
    expect(service).toBeTruthy();
  }));
});
