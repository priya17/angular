import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PoweredgeComponent } from './poweredge/poweredge.component';
import { AppComponent } from './app.component';
import { XpsComponent } from './xps/xps.component';
const routes: Routes = [
{
  path: 'xps',
  component: XpsComponent,
},
{
  path: 'poweredge',
  component: PoweredgeComponent,
},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
