import { Component, OnInit } from '@angular/core';
import { DataService } from './data.service';
import { Observable } from 'rxjs';
import { IProduct } from './model/ProductModel';
import { IComponentClassification } from './model/ComponentClassificationModel';
import { ItemComponents } from './model/ItemComponentsModel';
import { IComponent } from './model/ComponentModel';
import { RouterModule, Routes } from '@angular/router';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent{
  title = 'tst';
  constructor() { 
  }
  
  
  ngOnInit() {
    
  }
}
