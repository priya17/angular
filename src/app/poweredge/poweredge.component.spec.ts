import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PoweredgeComponent } from './poweredge.component';

describe('PoweredgeComponent', () => {
  let component: PoweredgeComponent;
  let fixture: ComponentFixture<PoweredgeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PoweredgeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PoweredgeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
