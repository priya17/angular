import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { Observable } from 'rxjs';
import { IProduct } from '../model/ProductModel';
import { IComponentClassification } from '../model/ComponentClassificationModel';
import { ItemComponents } from '../model/ItemComponentsModel';
import { IComponent } from '../model/ComponentModel';
import { RouterModule, Routes } from '@angular/router';
import { DatapService } from '../datap.service';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  title = 'tst';
  products: IProduct;
  itemComp: ItemComponents[];
  comps: IComponent;
  i$: number;
  _listFilter: string;
  i = 0;
  get listFilter(): string 
  {
    return this._listFilter;
  }
  set listFilter(value: string)
  {
    this._listFilter = value;
    this.itemComp = this.listFilter ? this.performFilter(this.listFilter) : this.products.itemComponents;

  }
  performFilter(filterBy: string): ItemComponents[] {
    filterBy = filterBy.toLocaleLowerCase();
    return this.itemComp.filter((product: ItemComponents) =>
      product.component.name.toLocaleLowerCase().indexOf(filterBy) != -1);
  }
  constructor() { }

  ngOnInit() {
  }

}
