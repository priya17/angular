import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IProduct } from './model/ProductModel';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private http: HttpClient) { }

  getData(): Observable <IProduct>
  {
    return this.http.get<IProduct>('../assets/jsonFiles/xps.json');
  }
}
