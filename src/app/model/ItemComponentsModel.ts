import { IComponent } from './ComponentModel';
import { IDuration } from './DurationModel';
import { IQuantity } from './QuantityModel';
import { IComponentClassification } from './ComponentClassificationModel';
export interface ItemComponents
{
    component: IComponent;
    isSelected: boolean;
    duration: IDuration;
    quantity: IQuantity;
    componentClassification: IComponentClassification;
}