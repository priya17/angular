import { IServiceDuration } from './ServiceDurationModel';
import { IServiceLevel } from './ServiceLevelModel';
export interface IExtension
{
    serviceDuration: IServiceDuration;
    serviceLevel: IServiceLevel;
}