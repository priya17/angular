export interface IQuantity
{
    unitOfMeasure: string;
    minimum: number;
    maximum: number;
    default: number;
}