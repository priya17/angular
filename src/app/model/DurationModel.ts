export interface IDuration
{
    unitOfMeasure: string;
    minimum: number;
    maximum: number;
    default: number;
}