import { IQuantity } from "./QuantityModel";

export interface IComponentClassification
{
    id: string;
    name: string;
    status: string;
    quantity: IQuantity;
}