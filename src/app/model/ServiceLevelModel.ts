export interface IServiceLevel
{
    id: string;
    name: string;
}