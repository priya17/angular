export interface IValues
{
    name: string;
    value: string;
    unitOfMeasure: string;
}