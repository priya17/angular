export interface IServiceDuration{
    unitOfMeasure: string;
    minimum: number;
    maximum: number;
    default: number;
}