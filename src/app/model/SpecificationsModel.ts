import { IValues } from './ValuesModel';
export interface ISpecification
{
    id: string;
    name: string;
    description: string;
    values: IValues;
}