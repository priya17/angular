export interface IProperties
{
    key: string;
    status: string;
    description: string;
    value: string;
    name: string;
}