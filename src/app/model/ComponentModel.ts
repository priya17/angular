import { ItemComponents } from "./ItemComponentsModel";
import { ISpecification } from "./SpecificationsModel";
import { IProperties } from "./PropertiesModel";

export interface IComponent
{
    isSellable: boolean;
    itemComponents: ItemComponents;
    commodityClassId: string;
    commodityClassName: string;
    manufacturerId: string;
    manufacturerName: string;
    manufacturerItemNumber: string;
    countryOfOriginCode: string;
    countryOfOrigin: string;
    specifications: ISpecification;
    definitionFromDateTime: string;
    definitionToDateTime: string;
    id: string;
    name: string;
    description: string;
    Properties: IProperties;
}